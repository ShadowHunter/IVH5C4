package ivh5.repository;

import ivh5.model.Therapist;
import java.sql.Date;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Maps data from the database on therapist objects in the model
 * @author lenna
 */
public class TherapistRowMapper implements RowMapper<Therapist>
{
    /**
     * maps the data from the resultset on therapist objects
     * 
     * @param rs The resultset that the database returns
     * @param rowNum the number of the current row
     * @return A therapist object
     * @throws SQLException An exception that is thrown if there is an error in the database,
     * while writing or reading data from database
     */
    @Override
    public Therapist mapRow(ResultSet rs, int rowNum) throws SQLException {
        Therapist therapist = new Therapist();
        therapist.setEmployeeNumber(rs.getInt("EmployeeNumber"));
        therapist.setName(rs.getString("Name"));
        therapist.setAddress(rs.getString("Address"));
        therapist.setPostcode(rs.getString("Postcode"));
        therapist.setCity(rs.getString("City"));     
        therapist.setDateOfBirth(dateToString(rs.getDate("DateOfBirth")));
        therapist.setPhone(rs.getString("Phonenumber"));
        therapist.setEmail(rs.getString("Email"));
        therapist.setHours(rs.getInt("Hours"));
        therapist.setSalary(rs.getDouble("Salary"));
        therapist.setBsn(rs.getInt("Bsn"));
        therapist.setMonday(rs.getBoolean("Monday"));
        therapist.setTuesday(rs.getBoolean("Tuesday"));
        therapist.setWednesday(rs.getBoolean("Wednesday"));
        therapist.setThursday(rs.getBoolean("Thursday"));
        therapist.setFriday(rs.getBoolean("Friday"));
        
        return therapist;
    }
    
    /**
     * Converts a SQL date to a String
     * 
     * @param date An SQL date
     * @return A string that contains the date
     */
    private String dateToString(Date date) {
 
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String convertedDate = df.format(date);
        
        return convertedDate;
    }
}
