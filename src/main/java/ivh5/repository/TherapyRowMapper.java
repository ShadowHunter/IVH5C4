package ivh5.repository;

import ivh5.model.Therapy;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 * Created by rubie_000 on 5-10-2016.
 */
public class TherapyRowMapper implements RowMapper<Therapy> {

    @Override
    public Therapy mapRow(ResultSet rs, int rowNum) throws SQLException {
        Therapy therapy = new Therapy();
        therapy.setId(rs.getInt("id"));
        therapy.setName(rs.getString("name"));
        therapy.setBSN(rs.getInt("BSN"));

        return therapy;
    }

}
