/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ivh5.repository;

import ivh5.model.Planning;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.jdbc.core.RowMapper;

/**
 *The resultsets from the overviewrepository queries will be handled here
 * 
 * @author Erik Koolen
 */
class OverviewRowMapper implements RowMapper<Planning> {

    /**
     * puts the data from the resultset into a planning object
     * 
     * @param rs the resultset that the databse returns
     * @param rowNum the number of the row
     * @return returns a planning object 
     * @throws SQLException An exception that is thrown if there is an error in the sql query,
     * making it not possible to return the requested data
     */
    @Override
    public Planning mapRow(ResultSet rs, int rowNum) throws SQLException {
        Planning planning = new Planning();
        Date startDate = null;  
        Date endDate = null;
        
        // make a correct date from the database datetime
        Timestamp startStamp = rs.getTimestamp("startDate");
        if(startStamp != null)
            startDate = new Date(startStamp.getTime());
        
        Timestamp endStamp = rs.getTimestamp("endDate");
        if(endStamp != null){
            endDate = new Date(endStamp.getTime());
        }
        
        String physioName = rs.getString("Name");
        int clientBSN = rs.getInt("clientBSN");
        int sessionID = rs.getInt("id");
        
        //dateformats to extract the date from the database into 3 categories
        SimpleDateFormat formatDay = new SimpleDateFormat("EE");
        SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");
        
        String day = formatDay.format(startDate);
        String date = formatDate.format(startDate);
        String time = formatTime.format(startDate);
        String endTime = formatTime.format(endDate);
        
        //input the correct values in planning
        planning.setDay(day);
        planning.setDate(date);
        planning.setStartTime(time);
        planning.setEndTime(endTime);
        planning.setPhysioName(physioName);
        planning.setClientBSN(clientBSN);
        planning.setSessionID(sessionID);
                        
        return planning;
    }   
}

class C_OverviewRowMapper implements RowMapper {
       
    /**
     * puts the data from the resultset into an int
     * 
     * @param rs the resultset that the databse returns
     * @param rowNum the number of the row
     * @return returns the total count in int form
     * @throws SQLException An exception that is thrown if there is an error in the sql query,
     * making it not possible to return the requested data
     */
    @Override
    public Integer mapRow(ResultSet rs, int rowNum) throws SQLException{
        int result;
        
        result = rs.getInt("COUNT(id)");
        
        return result;
    }
}
