/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ivh5.repository;

import ivh5.model.FysioPractice;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author Rico
 */
class FysioPracticeRowMapper implements RowMapper<FysioPractice>{
        @Override     
        public FysioPractice mapRow(ResultSet rs, int rowNum) throws SQLException {
            FysioPractice fysioPractice = new FysioPractice();         
            fysioPractice.setId(rs.getInt("id"));         
            fysioPractice.setName(rs.getString("name"));  
            fysioPractice.setAddress(rs.getString("address")); 
            fysioPractice.setPostcode(rs.getString("postcode")); 
            fysioPractice.setCity(rs.getString("city")); 
            fysioPractice.setPhoneNumber(rs.getString("phoneNumber")); 
            fysioPractice.setEmail(rs.getString("email")); 
            return fysioPractice;     
        }     
}
