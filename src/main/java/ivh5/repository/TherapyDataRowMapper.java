package ivh5.repository;

import ivh5.model.TherapyData;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by rubie_000 on 14-10-2016.
 */
public class TherapyDataRowMapper implements RowMapper<TherapyData> {

    @Override
    public TherapyData mapRow(ResultSet rs, int rowNum) throws SQLException {
        TherapyData therapyData = new TherapyData();
        therapyData.setTherapyCode(rs.getInt("code"));
        therapyData.setTherapyName(rs.getString("name"));
        therapyData.setSessionNumber(rs.getInt("sessionNumber"));
        therapyData.setSessionlenght(rs.getDouble("sessionLenght"));
        therapyData.setTherapyRate(rs.getDouble("rate"));

        return therapyData;
    }
}
