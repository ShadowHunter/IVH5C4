/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ivh5.repository;

import ivh5.model.FysioPractice;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Rico
 */
@Repository
public class FysioPracticeRepository {
    private final Logger logger = LoggerFactory.getLogger(TherapyRepository.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    public int checkCount(){
        final String sql = "SELECT COUNT(*) FROM fysiopractice";
        int count;
        count = jdbcTemplate.queryForObject(sql, Integer.class);
        return count;
    }
    
    //Create fysiopractice with id 1
    public void create(final FysioPractice fysioPractice)  {
            
        if(checkCount() == 0){
            
            final String sql = "INSERT INTO fysiopractice(`id`,`name`,`address` ,`postcode`,`city`,`phoneNumber`,`email`) " +
                "VALUES(1,?,?,?,?,?,?)";
            try{
                jdbcTemplate.update(new PreparedStatementCreator() {

                    @Override
                    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                        PreparedStatement ps = connection.prepareStatement(sql);
                        ps.setString(1, fysioPractice.getName());
                        ps.setString(2, fysioPractice.getAddress());
                        ps.setString(3, fysioPractice.getPostcode());
                        ps.setString(4, fysioPractice.getCity());
                        ps.setString(5, fysioPractice.getPhoneNumber());
                        ps.setString(6, fysioPractice.getEmail());
                        return ps;
                    }
                }); 
            }
            catch(Exception exception){
                logger.debug("create FysioPractice - " + exception.getMessage());
            }
        }
    }
    
    //Update fysiopractice
    public void update(final FysioPractice fysioPractice)  {
        
        final String sql = "UPDATE fysiopractice SET `id`=?, `name`=?,`address`=? ,`postcode`=?,`city`=?,`phoneNumber`=?,`email`=? WHERE `id`=?";
        try{
            jdbcTemplate.update(new PreparedStatementCreator() {

                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(sql);
                    ps.setInt(1, fysioPractice.getId());
                    ps.setString(2, fysioPractice.getName());
                    ps.setString(3, fysioPractice.getAddress());
                    ps.setString(4, fysioPractice.getPostcode());
                    ps.setString(5, fysioPractice.getCity());
                    ps.setString(6, fysioPractice.getPhoneNumber());
                    ps.setString(7, fysioPractice.getEmail());
                    ps.setInt(8, fysioPractice.getId());
                    return ps;
                }
            }); 
        }
        catch(Exception exception){
            logger.debug("update FysioPractice - " + exception.getMessage());
        }
    }
    
    //Find fysiopractice by id    
   public FysioPractice findFysioPracticeById(int id) {               
       return jdbcTemplate.queryForObject(                 
               "SELECT * FROM fysiopractice WHERE id=?",                 
               new Object[]{id},new FysioPracticeRowMapper());     
   }
}
