package ivh5.repository;

import ivh5.model.TherapySession;

import java.sql.ResultSet;
import java.sql.SQLException;

import ivh5.model.TherapyStatus;
import org.springframework.jdbc.core.RowMapper;

/**
 * Created by rubie_000 on 5-10-2016.
 */
public class SessionRowMapper implements RowMapper<TherapySession> {

    @Override
    public TherapySession mapRow(ResultSet rs, int rowNum) throws SQLException {
        TherapySession therapySession = new TherapySession();
        therapySession.setId(rs.getInt("id"));
        therapySession.setStartDate(rs.getTimestamp("startDate"));
        therapySession.setEndDate(rs.getTimestamp("endDate"));
        therapySession.setClientBSN(rs.getInt("clientBSN"));
        therapySession.setTherapyStatus(TherapyStatus.valueOf(rs.getString("status")));
        therapySession.setTherapistBSN(rs.getInt("therapistBSN"));


        return therapySession;
    }

}
