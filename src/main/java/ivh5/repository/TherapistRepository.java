/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ivh5.repository;

import ivh5.model.Therapist;

import java.sql.*;
import java.sql.Date;
import java.text.DateFormatSymbols;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Handles data that needs to be retrieved from the database or that needs to be
 * sent to the database
 *
 * @author lennard Slabbekoorn
 */
@Repository
public class TherapistRepository {
    private final Logger logger = LoggerFactory.getLogger(TherapistRepository.class);
    ;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Inserts a new therapist in the database
     *
     * @param therapist The therapist that needs to be inserted into the database
     * @return the therapist that was inserted
     * @Author Lennard Slabbekoorn
     */
    //insert the newly created therapist
    public Therapist insertTherapist(final Therapist therapist) {

        final String sql = "INSERT INTO physiotherapist(`Name`, `Address`, `Postcode`, `City`, `DateOfBirth`, `Phonenumber`, `Email`, `Hours`, `Salary`, `Bsn`, `Monday`, `Tuesday`, `Wednesday`, `Thursday`, `Friday`)" +
                "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        jdbcTemplate.update(new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, therapist.getName());
                ps.setString(2, therapist.getAddress());
                ps.setString(3, therapist.getPostcode());
                ps.setString(4, therapist.getCity());
                ps.setString(6, therapist.getPhone());
                ps.setString(7, therapist.getEmail());
                ps.setInt(8, therapist.getHours());
                ps.setDouble(9, therapist.getSalary());
                ps.setInt(10, therapist.getBsn());
                ps.setBoolean(11, therapist.getMonday());
                ps.setBoolean(12, therapist.getTuesday());
                ps.setBoolean(13, therapist.getWednesday());
                ps.setBoolean(14, therapist.getThursday());
                ps.setBoolean(15, therapist.getFriday());
                try {
                    ps.setDate(5, convertDate(therapist.getDateOfBirth()));
                } catch (ParseException ex) {
                }

                return ps;
            }
        });
        return therapist;
    }

    /**
     * updates a therapist in the database
     *
     * @param therapist The therapist that needs to be updated
     * @return The therapist that was inserted
     * @Author Lennard Slabbekoorn
     */
    //update existing therapist
    public Therapist updateTherapist(Therapist therapist) {

        final String sql = "UPDATE physiotherapist SET `Name`=?, `Address`=?, `Postcode`=?, `City`=?, `DateOfBirth`=?, `Phonenumber`=?, `Email`=?, `Hours`=?, `Salary`=?, `Bsn`=?, `Monday`=?, `Tuesday`=?, `Wednesday`=?, `Thursday`=?, `Friday`=? WHERE `EmployeeNumber`=?";

        jdbcTemplate.update(new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, therapist.getName());
                ps.setString(2, therapist.getAddress());
                ps.setString(3, therapist.getPostcode());
                ps.setString(4, therapist.getCity());
                ps.setString(6, therapist.getPhone());
                ps.setString(7, therapist.getEmail());
                ps.setInt(8, therapist.getHours());
                ps.setDouble(9, therapist.getSalary());
                ps.setInt(10, therapist.getBsn());
                ps.setBoolean(11, therapist.getMonday());
                ps.setBoolean(12, therapist.getTuesday());
                ps.setBoolean(13, therapist.getWednesday());
                ps.setBoolean(14, therapist.getThursday());
                ps.setBoolean(15, therapist.getFriday());
                ps.setInt(16, therapist.getEmployeeNumber());
                try {
                    ps.setDate(5, convertDate(therapist.getDateOfBirth()));
                } catch (ParseException ex) {
                }

                return ps;
            }
        });
        return therapist;
    }

    /**
     * Deletes a therapist in the database
     *
     * @param employeeNumber the employeeNumber of the therapist that needs to
     *                       be deleted
     * @throws SQLException An exception that is thrown when the database cannot
     *                      delete the specified therapist
     * @Author Lennard Slabbekoorn
     */
    //delete therapist
    public void deleteTherapist(int employeeNumber) throws SQLException {
        jdbcTemplate.update("DELETE FROM physiotherapist WHERE EmployeeNumber=?", new Object[]{employeeNumber});
    }

    /**
     * Selects All the therapists in the database
     *
     * @return An ArrayList of therapists
     * @Author Lennard Slabbekoorn
     */
    //find all therapists
    @Transactional(readOnly = true)
    public List<Therapist> findAllTherapists() {
        List<Therapist> result = new ArrayList();
        try {
            result = jdbcTemplate.query("SELECT * FROM physiotherapist", new TherapistRowMapper());
        } catch (Exception exception) {
            throw exception;
        }
        return result;
    }

    /**
     * Selects the therapist with the specified employeeNumber
     *
     * @param employeeNumber The employeeNumber of the therapist that needs to be deleted
     * @return the therapist with the specified employeeNumber
     * @Author Lennard Slabbekoorn
     */
    //find therapist by number
    @Transactional(readOnly = true)
    public Therapist findTherapistByNumber(int employeeNumber) {
        return jdbcTemplate.queryForObject(
                "SELECT * FROM physiotherapist WHERE EmployeeNumber=?",
                new Object[]{employeeNumber}, new TherapistRowMapper());
    }

    /**
     * converts a String in an SQL date
     *
     * @param date The String containting the date that needs to be parsed
     * @return an SQLdate
     * @throws ParseException this exception is thrown when the String cannot
     *                        be parsed into an SQLdate
     * @Author Lennard Slabbekoorn
     */
    public static java.sql.Date convertDate(String date) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        java.util.Date javaDate = formatter.parse(date);
        java.sql.Date sqlDate = new java.sql.Date(javaDate.getTime());

        return sqlDate;
    }


    public Boolean therapistWorking(int therapistBSN, java.util.Date startDate) {
        Format formatter = new SimpleDateFormat("EEEE");
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTime(startDate);
        String dayName = formatter.format(cal.getTime());
        int count;
        switch (dayName) {
            case "maandag":
                dayName = "Monday";
                break;
            case "dinsdag":
                dayName = "Tuesday";
                break;
            case "woensdag":
                dayName = "Wednesday";
                break;
            case "donderdag":
                dayName = "Thursday";
                break;
            case "vrijdag":
                dayName = "Friday";
                break;
            default:
               return false;
        }
        final String therapistWorkingSql = "SELECT count(*) FROM physiotherapist where Bsn = ? and " + dayName + " = 1";
        try {
            count = jdbcTemplate.queryForObject(therapistWorkingSql, new Object[]{therapistBSN},Integer.class);
        } catch (DataAccessException e) {
            throw new RuntimeException(e);
        }
        if (count == 1) {
            return true;
        }
        return false;
    }
}
