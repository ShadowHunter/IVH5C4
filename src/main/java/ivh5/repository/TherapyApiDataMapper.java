package ivh5.repository;

import ivh5.model.TherapyApiData;


import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 * Created by rubie_000 on 22-10-2016.
 */
public class TherapyApiDataMapper implements RowMapper<TherapyApiData> {

    public TherapyApiData mapRow(ResultSet rs, int rowNum) throws SQLException {
        TherapyApiData therapyApiData= new TherapyApiData();
        therapyApiData.setClientBSN(rs.getInt("clientBSN"));
        //therapyApiData.setTherapyCode(rs.getInt("therapyCode"));
        //therapyApiData.setPhysioPracticeNumber(rs.getInt("fysioPracticeNumber"));
        therapyApiData.setStartDate(rs.getDate("startDate"));
        therapyApiData.setEndDate(rs.getDate("endDate"));
        return therapyApiData;
    }
}
