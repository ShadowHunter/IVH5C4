package ivh5.repository;

import ivh5.model.TherapyApiData;
import ivh5.model.TherapySession;
import ivh5.model.TherapyData;
import ivh5.model.TherapyStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.InvalidResultSetAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.sql.Date;
import java.util.*;

/**
 * Created by rubie_000 on 9-10-2016.
 */
@Repository
public class TherapyRepository {
    private final Logger logger = LoggerFactory.getLogger(TherapyRepository.class);;
    private KeyHolder keyHolder = new GeneratedKeyHolder();
    private int count;
    private int id;
    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    private JdbcTemplate jdbcTemplate;


    /**
     * Create a new therapysession
     * @param therapySession The model from the Springframework
     */

    public void create(final TherapySession therapySession)  {
        logger.debug("insert a therapysession");
        int therapyNumber = checkExistingTherapy(therapySession);


        final String sql = "INSERT INTO session(`startDate`, `endDate` , `therapistBSN`, `therapy1_id`) " +
                "VALUES(?,?,?,?)";

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(sql);
            Timestamp startDate = new Timestamp(therapySession.getStartDate().getTime());
            Timestamp endDate = new Timestamp(therapySession.getEndDate().getTime());
            ps.setTimestamp(1, startDate);
            ps.setTimestamp(2, endDate);
            ps.setInt(3, therapySession.getTherapistBSN());
            ps.setInt(4, therapyNumber);
            return ps;
        });

    }

    /**
     * Get all therapiesdata from database
     * @return a list with therapydata
     */
    public List<TherapyData> getAllTherapiesData() {
        final String sql = "SELECT * FROM `therapydata`";
        List<TherapyData> therapiesData = new ArrayList();
        try {
            therapiesData = jdbcTemplate.query(sql, new TherapyDataRowMapper());
        } catch (Exception exception) {
            logger.debug("getAllTherapiesData - " + exception.getMessage());
        }

        return therapiesData;
    }


    /**
     * Check if session can be planned on specific date.
     * @param startDate
     * @param endDate
     * @param therapistBSN
     * @return a boolean
     */
    public Boolean checkAvailableDate(java.util.Date startDate,java.util.Date endDate, int therapistBSN){
        final String sql = "SELECT count(*) FROM `session` WHERE ? >= `startDate` and ? <= `endDate` and therapistBSN = ?";
        final String checkDate = "SELECT count(*) FROM `session` WHERE ? <= `startDate` and ? >= `startDate` and therapistBSN = ?";
        int countDate = 0;
        try {
            count = jdbcTemplate.queryForObject(sql, new Object[]{startDate,startDate,therapistBSN},Integer.class);
            countDate = jdbcTemplate.queryForObject(checkDate, new Object[]{startDate,endDate,therapistBSN},Integer.class);
        } catch (Exception exception) {
            logger.debug("checkAvailableDate - " + exception.getMessage());
        }

        if(count == 0 && countDate == 0) {
            return true;
        }
        return false;
    }

    /**
     * Check if there is already an existing therapy, if there is one update the sessionNumber and create a new session.
        If not create new therapy and session with foreign key to therapy.id
     * @param therapySession
     */

    //Check if there is already an existing therapy, if there is one update the sessionNumber and create a new session.
    // If not create new therapy and session with foreign key to therapy.id
    public int checkExistingTherapy(TherapySession therapySession){
        final String getTherapyCount = "SELECT count(*) FROM `therapy` WHERE `clientBSN` = ? and `therapyCode` = ? and `status` = 'IN_BEHANDELING'";

        try {
            count = jdbcTemplate.queryForObject(getTherapyCount, new Object[]{therapySession.getClientBSN(), therapySession.getTherapyCode1()},Integer.class);
        } catch (Exception exception) {
            logger.debug("checkExistingTherapy - " + exception.getMessage());
        }
        if(count == 0){
            //INSERT NEW THERAPY
            return id = createNewTherapy(therapySession);

        }
        else{
            if(!checkForLastSession(therapySession)) {
                //UPDATE THERAPYSESSIONNUMBER
                return id = increaseSessionNumber(therapySession);
            }
            else{
                return id = createNewTherapy(therapySession);
            }
        }
    }
    /**
     * Check therapy table if the sessionnumber equals the number from therapydata
     * @param therapySession
     * @return a boolean
     */

    public Boolean checkForLastSession(TherapySession therapySession){
        final String getEqualSessionNumber = "SELECT count(*) FROM `therapy`,`therapydata` WHERE `clientBSN` = ? and `therapyCode` = ? and `status` = 'IN_BEHANDELING' and therapy.therapyCode = therapydata.code and therapy.sessionNumber = therapydata.sessionNumber";
        int sessionCount;

        sessionCount = jdbcTemplate.queryForObject(getEqualSessionNumber, new Object[]{therapySession.getClientBSN(), therapySession.getTherapyCode1()},Integer.class);
        if(sessionCount != 0){
            setTherapyStatus(therapySession);
            return true;
        }
        return false;

    }

    /**
     * Set new therapystatus to Afgerond
     * @param therapySession
     *
     */
    public void setTherapyStatus(TherapySession therapySession){
        final String updateTherapyStatus = "UPDATE therapy SET `status` = 'AFGEROND', `endDate` = ? WHERE `clientBSN` = ? and `therapyCode` = ? and `status` = 'IN_BEHANDELING'";
        try {
            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(updateTherapyStatus);
                Date endDate = new Date(therapySession.getStartDate().getTime());
                ps.setDate(1,endDate);
                ps.setInt(2, therapySession.getClientBSN());
                ps.setInt(3, therapySession.getTherapyCode1());
                return ps;
            });

        } catch (Exception exception) {
            logger.debug("setTherapyStatus - " + exception.getMessage());
        }
    }


    /**
     * Create new therapy based on therapysession
     * @param therapySession
     * @return an id for therapysession
     */
    public int createNewTherapy(TherapySession therapySession){
        final String createNewTherapy ="INSERT INTO therapy(`clientBSN`, `therapyCode` , `sessionNumber`, `startDate`) " +
                "VALUES(?,?,?,?)";
        try {

            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(createNewTherapy);
                Date startDate = new Date(therapySession.getStartDate().getTime());
                ps.setInt(1, therapySession.getClientBSN());
                ps.setInt(2, therapySession.getTherapyCode1());
                ps.setInt(3, 1);
                ps.setDate(4, startDate);
                return ps;
            },keyHolder);
            id = keyHolder.getKey().intValue();

        } catch (Exception exception) {
            logger.debug("createNewTherapy - " + exception.getMessage());
        }
        checkForLastSession(therapySession);

        return id;
    }

    /**
     * Update sessionNumber from therapy
     * @param therapySession
     * @return an id for therapysession
     */
    public int increaseSessionNumber(TherapySession therapySession){
        final String getTherapyId = "SELECT id FROM `therapy` WHERE `clientBSN` = ? and `therapyCode` = ? and `status` = 'IN_BEHANDELING'";
        final String updateSessionNumber = "UPDATE therapy SET `sessionNumber` = `sessionNumber` + 1 WHERE `clientBSN` = ? and `therapyCode` = ? and `status` = 'IN_BEHANDELING'";
        try {
            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(updateSessionNumber);
                ps.setInt(1, therapySession.getClientBSN());
                ps.setInt(2, therapySession.getTherapyCode1());
                return ps;
            });

            id = jdbcTemplate.queryForObject(getTherapyId, new Object[]{therapySession.getClientBSN(), therapySession.getTherapyCode1()},Integer.class);
        } catch (Exception exception) {
            logger.debug("increaseSessionNumber - " + exception.getMessage());
        }
        checkForLastSession(therapySession);
        return id;
    }

    /**
     * Get the correct apidata based on bsn,year and month
     * @param  BSN
     * @param  year
     * @param month
     * @return a list with therapyapidata
     */
    public List<TherapyApiData> getTherapyApiData(int BSN, int year, int month){
        return jdbcTemplate.query(
                "SELECT * FROM `therapy` WHERE clientBSN = ? and status = 'AFGEROND' and YEAR(endDate) = ? and MONTH(endDate) = ? ",
                new Object[]{BSN,year,month}, new TherapyApiDataMapper());

    }

    /**
     * Get all therapycodes for api
     * @param  BSN
     * @param  year
     * @param month
     * @return a list with therapycodes
     */
    public List<Integer> getTherapyCodes(int BSN, int year, int month) {
        return jdbcTemplate.queryForList(
                "SELECT therapyCode FROM `therapy` WHERE clientBSN = ? and status = 'AFGEROND' and YEAR(endDate) = ? and MONTH(endDate) = ? ",
                new Object[]{BSN,year,month}, Integer.class);
    }

    /**
     * Get Therapysession by inserted id
     * @param  id
     * @return a session based on id
     */
    public TherapySession getSessionById(int id){
        return jdbcTemplate.queryForObject(
                "SELECT session.id,session.status,session.startDate,session.endDate,session.therapistBSN,therapy.clientBSN FROM `session`,`therapy` WHERE session.id = ? and session.therapy1_id = therapy.id",
                new Object[]{id}, new SessionRowMapper());
    }

    /**
     * Update the therapysession based on an input
     * @param  therapySession
     */
    public void updateTherapySession(TherapySession therapySession){
        final String updateTherapySessionSQL = "UPDATE session,therapy SET session.status = ?, session.startDate = ?, session.endDate = ?, session.therapistBSN = ?,therapy.clientBSN = ? WHERE session.id = ? and therapy.id = ? ";
        count = getSessionTherapyId(therapySession.getId());
        try {
            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(updateTherapySessionSQL);
                Timestamp startDate = new Timestamp(therapySession.getStartDate().getTime());
                Timestamp endDate = new Timestamp(therapySession.getEndDate().getTime());
                ps.setString(1, therapySession.getTherapyStatus().toString());
                ps.setTimestamp(2, startDate);
                ps.setTimestamp(3, endDate);
                ps.setInt(4, therapySession.getTherapistBSN());
                ps.setInt(5, therapySession.getClientBSN());
                ps.setInt(6,therapySession.getId());
                ps.setInt(7, count);
                return ps;
            });

        } catch (Exception exception) {
            logger.debug("updateTherapySession - " + exception.getMessage());
        }

    }
    /**
     * Get therapysession foreign key id
     * @param  id
     * @return an id for therapysession
     */
    public int getSessionTherapyId(int id){
        final String selectSessionTherapyId = "SELECT therapy1_id FROM `session` WHERE id = ?  ";
        return jdbcTemplate.queryForObject(selectSessionTherapyId, new Object[]{id},Integer.class);
    }
    /**
     * Check if a date can be planned for edit. Got extra id paramater
     * @param  startDate
     * @param  endDate
     * @param  id
     * @param  therapistBSN
     * @return a boolean
     */
    public Boolean checkAvailableDateForEdit(java.util.Date startDate,java.util.Date endDate, int id,int therapistBSN){
        final String sql = "SELECT count(*) FROM `session` WHERE ? >= `startDate` and ? <= `endDate` and therapistBSN = ? and `id` <> ?  ";
        final String checkDate = "SELECT count(*) FROM `session` WHERE ? <= `startDate` and ? >= `startDate` and therapistBSN = ? and `id` <> ?  ";
        int countDate = 0;

        try {
            count = jdbcTemplate.queryForObject(sql, new Object[]{startDate,startDate,therapistBSN,id},Integer.class);
            countDate = jdbcTemplate.queryForObject(checkDate, new Object[]{startDate,endDate,therapistBSN,id},Integer.class);

        } catch (Exception exception) {
            logger.debug("checkAvailableDateForEdit - " + exception.getMessage());
        }
        if(count == 0 && countDate == 0) {
            return true;
        }
        return false;
    }
    /**
     * Delete an therapysession based on id
     * @param  sessionId
     */
    public void deleteSessionById(int sessionId){
        final String removeSessionSql = "DELETE FROM session WHERE `id` = ? ";
        try {
            jdbcTemplate.update(removeSessionSql, new Object[]{sessionId});
        } catch (Exception exception)
        {
            logger.debug("deleteSessionById - " + exception.getMessage());
        }
    }
    /**
     * Delete a therapy from a session.
     * @param  therapyId
     */
    public void deleteTherapyBySession(int therapyId){
        final String removeTherapySql = "DELETE FROM therapy WHERE `id` = ? ";
        try {
            jdbcTemplate.update(removeTherapySql, new Object[]{therapyId});
        } catch (Exception exception)
        {
            logger.debug("deleteTherapyBySession - " + exception.getMessage());
        }
    }
    /**
     * Checks if therapy sessionnumber = 1 or > 1. So it can deletes therapy if 1
     * @param  therapyId
     * @return a boolean
     */
    public Boolean therapySessionNumberIsOne(int therapyId){
        final String removeTherapySql = "SELECT count(*) FROM therapy WHERE `id` = ? and `sessionNumber` = ? ";
        try {
            count =  jdbcTemplate.queryForObject(removeTherapySql, new Object[]{therapyId,1}, Integer.class);
        }catch (Exception exception)
        {
            logger.debug("therapySessionNumberIsOne - " + exception.getMessage());
        }
        if(count == 0) {
            return false;
        }
        return true;
    }
    /**
     * Decrease session number by 1 from therapy
     * @param  therapyId
     */
    public void decreaseSessionNumber(int therapyId){
        final String decreaseSessionNumberSql = "UPDATE therapy SET `sessionNumber` = `sessionNumber` + -1 WHERE `id` = ?";
        try {
            jdbcTemplate.update(decreaseSessionNumberSql, new Object[]{therapyId});
        } catch (Exception exception)
        {
            logger.debug("decreaseSessionNumber - " + exception.getMessage());
        }
    }
}