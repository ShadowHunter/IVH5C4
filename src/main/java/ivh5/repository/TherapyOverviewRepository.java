/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ivh5.repository;

import ivh5.model.Planning;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *Sends queries to the database and sends retrieved values back to the controller
 * 
 * @author Erik Koolen
 */
@Repository
public class TherapyOverviewRepository {
    
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    /**
     * searches and retrieves all the needed data to create a planning object
     * 
     * @param startdate search and retrieve data with this date as startpoint
     * @param enddate search and retrieve data with this date as endpoint
     * @return returns a list of planning objects
     */
    public List<Planning> findtherapy(LocalDate startdate, LocalDate enddate){
        
        List<Planning> result = new ArrayList<>();
        try{
            result = jdbcTemplate.query("SELECT physiotherapist.Name, therapy.clientBSN, session.startDate, session.endDate, session.id "
                    + "FROM `session`, `physiotherapist`, `therapy` "
                    + "WHERE session.therapistBSN = physiotherapist.Bsn AND session.therapy1_id = therapy.id "
                    + "AND session.startDate BETWEEN '"+ startdate +"' AND '" + enddate + "'", new OverviewRowMapper());
        } catch(Exception ex) {
            throw ex;
        }
        return result;
    }
    
    /**
     * counts all the rows between the two given parameters
     * 
     * @param startdate start counting rows from this parameter
     * @param enddate stop counting rows at this parameter
     * @return Return the total count
     */
    public int findalltherapies(LocalDate startdate, LocalDate enddate){
        int total;
        
        try{
            total = (Integer) jdbcTemplate.queryForObject("SELECT COUNT(id) FROM `session` WHERE `startDate` BETWEEN '"+ startdate +"' AND '" + enddate + "'",new C_OverviewRowMapper());
        }catch(Exception ex) {
            throw ex;
        }
        
        return total;
    }
}
