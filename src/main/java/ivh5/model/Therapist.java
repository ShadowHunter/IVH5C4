/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ivh5.model;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author lennard
 * 
 */
public class Therapist {
    //attributes
    @NotNull
    @DecimalMax(value="999999999", message="Het BSN moet uit 9 cijfers bestaan")
    @DecimalMin(value="100000001", message="Het BSN moet uit 9 cijfers bestaan")
    private int bsn;
    
    @NotNull
    private int employeeNumber;
    
    @NotNull
    @DecimalMin(value="1", message="Het minimum aantal uren is 1")
    private int hours;
    
    @NotNull
    @DecimalMin(value="0.01", message="Het salaris moet hoger zijn dan 0.01 en het komma-teken mag niet gebruikt worden")
    private double salary;
    
    @NotNull
    @Size(min=2, max=30, message="Vul een geldige naam in, de naam mag niet langer zijn dan 30 karakters ")
    private String name;
    
    @NotNull
    @NotEmpty(message="Voer een adres in")
    private String address;
    
    @NotNull
    @Pattern(regexp="^[0-9]{4}(?:[a-zA-z]{2})$", 
            message="Geen geldige postcode")
    private String postcode;
    
    @NotNull
    @Size(min=2, max=20, message="De woonplaats kan uit minimaal 2 en maximaal 20 karakters bestaan")
    private String city;
    
    @NotNull
    @Pattern(regexp="^(?:[0-9]{10})$",
            message="Voer een geldig telefoonnummer in, gebruik geen tekens")
    private String phone;
    
    @NotNull
    @Pattern(regexp="[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\."
            +"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@"
            +"(?:[a-zA-Z0-9](?:[a-z0-9-]*[a-zA-Z0-9])?\\.)+[a-z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?",
            message="Voer een geldig email adres in")
    private String email;
    
    @NotNull
    @Pattern(regexp="^(?:[0-9]{2})-(?:[0-9]{2})-(?:[0-9]{4})$",
            message="Voer een geldige geboortedatum in (dd-mm-jjjj)")
    private String dateOfBirth;
    
    @NotNull
    private boolean monday;
    @NotNull
    private boolean tuesday;
    @NotNull
    private boolean wednesday;
    @NotNull
    private boolean thursday;
    @NotNull
    private boolean friday;
    
    //constructor
    /**
     * Controller maps values of views on this constructor
     */
    public Therapist() {}
    
    public Therapist(int bsn, int employeeNumber, int hours, double salary, 
            String name, String address, String postcode, String city, String phone, String email, 
            String dateOfBirth, boolean monday, boolean tuesday, boolean wednesday, boolean thursday,
            boolean friday) {
        
        this.bsn = bsn;
        this.employeeNumber = employeeNumber;
        this.hours = hours;
        this.salary = salary;
        this.name = name;
        this.address = address;
        this.postcode = postcode;
        this.city = city;
        this.phone = phone;
        this.email = email;
        this.dateOfBirth = dateOfBirth;
        this.monday = monday;
        this.tuesday = tuesday;
        this.wednesday = wednesday;
        this.thursday = thursday;
        this.friday = friday;
    }
    
   //methods
   //getters
   public int getBsn() {
       return bsn;
   } 
   
   public int getEmployeeNumber() {
       return employeeNumber;
   }
   
   public int getHours() {
       return hours;
   }
   
   public String getName() {
       return name;
   }
   
   public double getSalary() {
       return salary;
   }
   
   public String getAddress() {
       return address;
   }
   
   public String getPostcode() {
       return postcode;
   }
   
   
   public String getCity() {
       return city;
   }
   
   public String getPhone() {
       return phone;
   }
   
   public String getEmail() {
       return email;
   }
   
   public String getDateOfBirth() {
       return dateOfBirth;
   }
   
   public boolean getMonday() {
       return monday;
   }
   
   public boolean getTuesday() {
       return tuesday;
   }
   
   public boolean getWednesday() {
       return wednesday;
   }
   
   public boolean getThursday() {
       return thursday;
   }
   
   public boolean getFriday() {
       return friday;
   }
   
   //setters
   public void setBsn(int bsn) {
       this.bsn = bsn;
   }
   
   public void setEmployeeNumber(int employeeNumber) {
       this.employeeNumber = employeeNumber;
   }
   
   public void setHours(int hours){
       this.hours = hours;
   }
   
   public void setSalary(double salary){
       this.salary = salary;
   }
   
   public void setWorkingDays(String[] workingdays){
   }
   
   public void setName(String name){
       this.name = name;
   }
   
   public void setAddress(String address) {
       this.address = address;
   }
   
   public void setPostcode(String postcode){
       this.postcode = postcode;
   }
   
   public void setCity(String city) {
       this.city = city;
   }
   
   public void setPhone(String phone) {
       this.phone = phone;
   }
   
   public void setEmail(String email) {
       this.email = email;
   }
   
   public void setDateOfBirth(String dateOfBirth){
       this.dateOfBirth = dateOfBirth;
   }
   
   public void setMonday(boolean monday){
       this.monday = monday;
   }
   
   public void setTuesday(boolean tuesday){
       this.tuesday = tuesday;
   }
   
   public void setWednesday(boolean wednesday){
       this.wednesday = wednesday;
   }
   
   public void setThursday(boolean thursday){
       this.thursday = thursday;
   }
   
   public void setFriday(boolean friday){
       this.friday = friday;
   }
}
