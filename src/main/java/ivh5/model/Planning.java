/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ivh5.model;

/**
 *
 * @author Erik Koolen
 */
public class Planning {
    
    private String day; 
    private String date;
    private String startTime;
    private String endTime;
    private String physioName;
    private int clientBSN;
    private int sessionID;
    
    public Planning (){
        
    }
    
    public String getDay(){
        return day;
    }
    
    public String getDate(){
        return date;
    }
    
    public String getStartTime(){
        return startTime;
    }
    
    public String getEndTime(){
        return endTime;
    }
    
    public String getPhysioName(){
        return physioName;
    }
    
    public int getClientBSN(){
        return clientBSN;
    }
    
    public int getSessionID(){
        return sessionID;
    }
    
    public void setDay(String day){
        this.day = day;
    }
    
    public void setDate(String date){
        this.date = date;
    }
    
    public void setStartTime(String startTime){
        this.startTime = startTime;
    }
    
    public void setEndTime(String endTime){
        this.endTime = endTime;
    }
    
    public void setPhysioName(String physioName){
        this.physioName = physioName;
    }
    
    public void setClientBSN(int clientBSN){
        this.clientBSN = clientBSN;
    }
    
    public void setSessionID(int sessionID){
        this.sessionID = sessionID;
    }
}
