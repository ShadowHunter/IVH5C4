package ivh5.model;

/**
 * Created by rubie_000 on 9-10-2016.
 */
public enum TherapyStatus {
    GEPLAND,
    UITGEVOERD,
    GECANCELD_DOOR_FYSIO,
    GECANCELD_DOOR_KLANT

}
