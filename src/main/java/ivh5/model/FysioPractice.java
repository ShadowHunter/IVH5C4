/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ivh5.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 *
 * @author Rico
 */
public class FysioPractice {
    private int id;
    @Pattern(regexp="^[a-zA-Z]+$",
            message="Voer een naam in, gebruik geen cijfers")
    private String name;
    private String address;
    private String postcode;
    @Pattern(regexp="^[a-zA-Z]+$",
            message="Voer een stad in, gebruik geen cijfers")
    private String city;
    @NotNull
    @Pattern(regexp="^(?:[0-9]{10})$",
            message="Voer een geldig telefoonnummer in, gebruik geen tekens")
    private String phoneNumber;
    @NotNull
    @Pattern(regexp="[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\."
            +"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@"
            +"(?:[a-zA-Z0-9](?:[a-z0-9-]*[a-zA-Z0-9])?\\.)+[a-z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?",
            message="Voer een geldig email adres in")
    private String email;
    
    public FysioPractice(){
        
    }
    
    public FysioPractice(int id, String name, String address, String postcode, String city, String phoneNumber, String email){
        this.id = id;
        this.name = name;
        this.address = address;
        this.postcode = postcode;
        this.city = city;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }
    
    //getters
    public int getId(){
        return id;
    }
    
    public String getName (){
        return name;
    }
    
    public String getAddress(){
        return address;
    }
    
    public String getPostcode(){
        return postcode;
    }
    
    public String getCity(){
        return city;
    }
    
    public String getPhoneNumber(){
        return phoneNumber;
    }
    
    public String getEmail(){
        return email;
    }
    
    //setters
    public void setId (int id) {
        this.id = id;
    }
    
    public void setName (String name){
        this.name = name;
    }
    
    public void setAddress (String address){
        this.address = address;
    }
    
    public void setPostcode (String postcode){
        this.postcode = postcode;
    }
    
    public void setCity (String city){
        this.city = city;
    }
    
    public void setPhoneNumber (String phoneNumber){
        this.phoneNumber = phoneNumber;
    }
    
    public void setEmail (String email){
        this.email = email;
    }
}
