/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ivh5.model;

import ivh5.model.Therapist;
import ivh5.controller.TherapyController;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 *
 * @author Erik
 */
public class Therapy {
    private TherapyController tController;

    private int id;
    @NotNull
    @Size(min = 1, max = 32)
    private String name;
    @NotNull
    private TherapyStatus therapyStatus;

    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date startDate;

    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date endDate;
    @NotNull
    private int therapyCode1;
    @NotNull
    private int therapyCode2;
    @NotNull
    private int therapyCode_N;
    @NotNull
    private int sessions;
    @NotNull
    private int BSN;
    @NotNull
    private Therapist therapist;

    public Therapy(){

    }

    public Therapy(int id, String name, TherapyStatus therapyStatus, Date startDate, Date endDate, int therapyCode1, int therapyCode2, int therapyCode_N, int sessions, int BSN, Therapist therapist) {
        this.id = id;
        this.name = name;
        this.therapyStatus = therapyStatus;
        this.startDate = startDate;
        this.endDate = endDate;
        this.therapyCode1 = therapyCode1;
        this.therapyCode2 = therapyCode2;
        this.therapyCode_N = therapyCode_N;
        this.sessions = sessions;
        this.BSN = BSN;
        this.therapist = therapist;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TherapyStatus getTherapyStatus() {
        return therapyStatus;
    }

    public void setTherapyStatus(TherapyStatus therapyStatus) {
        this.therapyStatus = therapyStatus;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getTherapyCode1() {
        return therapyCode1;
    }

    public void setTherapyCode1(int therapyCode1) {
        this.therapyCode1 = therapyCode1;
    }

    public int getTherapyCode2() {
        return therapyCode2;
    }

    public void setTherapyCode2(int therapyCode2) {
        this.therapyCode2 = therapyCode2;
    }

    public int getTherapyCode_N() {
        return therapyCode_N;
    }

    public void setTherapyCode_N(int therapyCode_N) {
        this.therapyCode_N = therapyCode_N;
    }

    public int getSessions() {
        return sessions;
    }

    public void setSessions(int sessions) {
        this.sessions = sessions;
    }

    public int getBSN() {
        return BSN;
    }

    public void setBSN(int BSN) {
        this.BSN = BSN;
    }

    public Therapist getTherapist() {
        return therapist;
    }

    public void setTherapist(Therapist therapist) {
        this.therapist = therapist;
    }


}
