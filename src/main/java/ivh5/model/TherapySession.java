/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ivh5.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 *
 * @author Erik
 */
public class TherapySession {

    private int id;

    private TherapyStatus therapyStatus;
    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private Date startDate;
    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private Date endDate;
    @NotNull
    private int therapyCode1;
    @NotNull
    @DecimalMax(value="999999999", message="Het BSN moet uit 9 cijfers bestaan")
    @DecimalMin(value="100000001", message="Het BSN moet uit 9 cijfers bestaan")
    private int clientBSN;
    @NotNull
    private int therapistBSN;

    public TherapySession(){

    }

    public TherapySession(int id, Date startDate, Date endDate, int therapistBSN ){
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.therapistBSN = therapistBSN;
    }

    public TherapySession(int id, TherapyStatus therapyStatus, Date startDate, Date endDate, int therapyCode1, int clientBSN, int therapistBSN) {
        this.id = id;
        this.therapyStatus = therapyStatus;
        this.startDate = startDate;
        this.endDate = endDate;
        this.therapyCode1 = therapyCode1;
        this.clientBSN = clientBSN;
        this.therapistBSN = therapistBSN;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TherapyStatus getTherapyStatus() {
        return therapyStatus;
    }

    public void setTherapyStatus(TherapyStatus therapyStatus) {
        this.therapyStatus = therapyStatus;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getTherapyCode1() {
        return therapyCode1;
    }

    public void setTherapyCode1(int therapyCode1) {
        this.therapyCode1 = therapyCode1;
    }


    public int getClientBSN() {
        return clientBSN;
    }

    public void setClientBSN(int clientBSN) {
        this.clientBSN = clientBSN;
    }

    public int getTherapistBSN() {
        return therapistBSN;
    }

    public void setTherapistBSN(int therapistBSN) {
        this.therapistBSN = therapistBSN;
    }


}
