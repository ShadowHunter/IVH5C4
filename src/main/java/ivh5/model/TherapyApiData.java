package ivh5.model;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by rubie_000 on 22-10-2016.
 */
public class TherapyApiData {
    private int clientBSN;
    private int physioPracticeNumber;
    private Date startDate;
    private Date endDate;
    private ArrayList<Integer> therapyCodes;

    public TherapyApiData(int clientBSN, int physioPracticeNumber, int therapyCode, Date startDate, Date endDate) {
        this.clientBSN = clientBSN;
        this.physioPracticeNumber = physioPracticeNumber;
        this.startDate = startDate;
        this.endDate = endDate;
        therapyCodes = new ArrayList<>();
    }

    public TherapyApiData() {
        therapyCodes = new ArrayList<>();
    }

    public TherapyApiData(int clientBSN, ArrayList<Integer> therapyCodes, Date startDate, Date endDate) {
    }

    public void addNewTherapyCode(int therapyCode){
       therapyCodes.add(therapyCode);
   }

    public ArrayList<Integer> getTherapyCodes() {
        return therapyCodes;
    }

    public void setTherapyCodes(ArrayList<Integer> therapyCodes) {
        this.therapyCodes = therapyCodes;
    }

    public int getClientBSN() {
        return clientBSN;
    }

    public void setClientBSN(int clientBSN) {
        this.clientBSN = clientBSN;
    }

    public int getPhysioPracticeNumber() {
        return physioPracticeNumber;
    }

    public void setPhysioPracticeNumber(int physioPracticeNumber) {
        this.physioPracticeNumber = physioPracticeNumber;
    }


    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
