package ivh5.model;

/**
 * Created by rubie_000 on 14-10-2016.
 */
public class TherapyData {

    private int therapyCode;
    private String therapyName;
    private int sessionNumber;
    private double sessionlenght;
    private double therapyRate;

    public TherapyData(int therapyCode, String therapyName, int sessionNumber, double sessionlenght, double therapyRate) {
        this.therapyCode = therapyCode;
        this.therapyName = therapyName;
        this.sessionNumber = sessionNumber;
        this.sessionlenght = sessionlenght;
        this.therapyRate = therapyRate;
    }

    public TherapyData() {
    }

    public int getTherapyCode() {
        return therapyCode;
    }

    public void setTherapyCode(int therapyCode) {
        this.therapyCode = therapyCode;
    }

    public String getTherapyName() {
        return therapyName;
    }

    public void setTherapyName(String therapyName) {
        this.therapyName = therapyName;
    }

    public int getSessionNumber() {
        return sessionNumber;
    }

    public void setSessionNumber(int sessionNumber) {
        this.sessionNumber = sessionNumber;
    }

    public double getSessionlenght() {
        return sessionlenght;
    }

    public void setSessionlenght(double sessionlenght) {
        this.sessionlenght = sessionlenght;
    }

    public double getTherapyRate() {
        return therapyRate;
    }

    public void setTherapyRate(double therapyRate) {
        this.therapyRate = therapyRate;
    }
}
