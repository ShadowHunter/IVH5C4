/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ivh5.config;


/**
 *
 * @author Rico
 */

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    
 @Autowired
 DataSource dataSource;
 
 @Autowired
 UserDetailsService userDetailsService;

 @Autowired
 public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {    
  auth.userDetailsService(userDetailsService).passwordEncoder(passwordencoder());;
   
 } 
 
   @Bean(name="userDetailsService")

    public UserDetailsService userDetailsService(){
     JdbcDaoImpl jdbcImpl = new JdbcDaoImpl();
     jdbcImpl.setDataSource((dataSource));
     jdbcImpl.setUsersByUsernameQuery("select username,password, enabled from users where username=?");
     jdbcImpl.setAuthoritiesByUsernameQuery("select b.username, a.role from user_roles a, users b where b.username=? and a.userid=b.userid");
     return jdbcImpl;
    }
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()

       .antMatchers("/","/therapist","/createtherapist","/therapist/edit","/therapist/delete","/fysiopractice/overview","/fysiopractice/create","/fysiopractice/edit").access("hasAnyRole('ROLE_ADMIN','ROLE_SECRETARESSE')")  
       .antMatchers("/","/overview","/therapy/create","/therapy/edit","/therapy/remove").access("hasAnyRole('ROLE_ADMIN','ROLE_SECRETARESSE','ROLE_THERAPIST')")  

       .and()
         .formLogin().loginPage("/login")
         .usernameParameter("username").passwordParameter("password")
       .and()
         .logout().logoutSuccessUrl("/login?logout") 
       .and()
        .exceptionHandling().accessDeniedPage("/403");
       //.and()
         //.csrf();
    }
    
    @Bean(name="passwordEncoder")
    public BCryptPasswordEncoder passwordencoder(){
     return new BCryptPasswordEncoder();
    }
        
    //dit stukje code NIET weggooien svp ivm security issues in database (ook niet bij mergeconflicten!)
    private CsrfTokenRepository csrfTokenRepository() 
{ 
    HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository(); 
    repository.setSessionAttributeName("_csrf");
    return repository; 
}
/*
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
            .inMemoryAuthentication()
                .withUser("admin").password("password").roles("ADMIN");

        auth
            .inMemoryAuthentication()
                .withUser("secretaresse").password("password").roles("SECRETARESSE");
        auth
            .inMemoryAuthentication()
                .withUser("therapist").password("password").roles("THERAPIST");



    }
*/
}