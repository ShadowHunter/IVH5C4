package ivh5.controller;

import ivh5.model.TherapyApiData;
import ivh5.model.TherapySession;
import ivh5.repository.TherapyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by rubie_000 on 20-10-2016.
 */
@RestController
public class ApiController {
    private TherapyRepository therapyRepository;


    @Autowired
    public ApiController(TherapyRepository therapyRepository){
        this.therapyRepository = therapyRepository;
    }

  
    @RequestMapping("/api/therapy/{year}/{month}/{BSN}")
    public TherapyApiData getTherapyDataByBSN(@PathVariable("BSN") int BSN, @PathVariable("year") int year, @PathVariable("month") int month) throws ParseException {


        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date startDate = sdf.parse("31-10-2900");
        Date endDate =  sdf.parse("31-10-1900");

        List<TherapyApiData> therapyApiData = therapyRepository.getTherapyApiData(BSN,year,month);
        List<Integer> therapyCodes;
        TherapyApiData tData = new TherapyApiData();
        tData.setPhysioPracticeNumber(1);

        for(TherapyApiData apiData : therapyApiData){
            tData.setClientBSN(apiData.getClientBSN());

            if(apiData.getStartDate().before(startDate)){
                startDate = apiData.getStartDate();
                tData.setStartDate(startDate);
            }

            if(apiData.getEndDate().after(endDate)){
                endDate = apiData.getEndDate();
                tData.setEndDate(endDate);
            }
        }

        therapyCodes = therapyRepository.getTherapyCodes(BSN,year,month);
        for(int tcodes :  therapyCodes){
            tData.addNewTherapyCode(tcodes);
            if(therapyCodes.indexOf(tcodes) == (therapyCodes.size() -1)){
                return tData;
            }
        }

        return tData;
    }
}
