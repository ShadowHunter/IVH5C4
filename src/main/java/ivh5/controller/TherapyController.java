package ivh5.controller;

import ivh5.model.TherapySession;
import ivh5.model.Therapist;
import ivh5.model.TherapyData;
import ivh5.model.TherapyStatus;
import ivh5.repository.TherapistRepository;
import ivh5.repository.TherapyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rubie_000 on 5-10-2016.
 */
@Controller
public class TherapyController {

    private final Logger logger = LoggerFactory.getLogger(TherapyController.class);;

    private TherapyRepository therapyRepository;
    private TherapistRepository therapistRepository;
    private TherapyData therapyData;
    private List<TherapyData> therapiesData;
    private List<Therapist> therapists;

    @Autowired
    public TherapyController(TherapyRepository therapyRepository,TherapistRepository therapistRepository){
        this.therapyRepository = therapyRepository;
        this.therapistRepository = therapistRepository;

    }

    @RequestMapping(value="/therapy/create", method = RequestMethod.GET)
    public String createNewTherapy(final TherapySession therapySession, final ModelMap model) {
        model.addAttribute("therapists", therapists = therapistRepository.findAllTherapists());
        model.addAttribute("therapiesData", therapiesData = therapyRepository.getAllTherapiesData());
        return "views/therapy/create";
    }

    @RequestMapping(value="/therapy/create", method = RequestMethod.POST)
    public String validateAndSaveTherapy(@Valid TherapySession therapySession, final BindingResult bindingResult, final ModelMap model,RedirectAttributes attributes) {
        model.addAttribute("therapists", therapists = therapistRepository.findAllTherapists());
        model.addAttribute("therapiesData", therapiesData = therapyRepository.getAllTherapiesData());

        if (bindingResult.hasErrors()) {
            logger.debug("validateAndSaveMember - not added, bindingResult.hasErrors");
            return "views/therapy/create";
        }

        if(therapistRepository.therapistWorking(therapySession.getTherapistBSN(),therapySession.getStartDate())){
            if(therapyRepository.checkAvailableDate(therapySession.getStartDate(),therapySession.getEndDate(), therapySession.getTherapistBSN())){
                therapyRepository.create(therapySession);
                model.addAttribute("success", "De behandeling is ingepland.");
                attributes.addFlashAttribute("success", "De behandeling is ingepland.");
            }
            else{
                model.addAttribute("error", "De behandeling kon niet worden ingepland, omdat er al een behandeling op dezelde datum en tijd staat gepland");
                return "views/therapy/create";
            }
        }
        else{
            model.addAttribute("error", "De therapeut is op deze dag niet aanwezig.");
            return "views/therapy/create";
        }

        return "redirect:/therapy/create";

    }

    @RequestMapping(value="/therapy/edit/{id}", method = RequestMethod.GET)
    public String editTherapy(ModelMap model, @PathVariable int id) {
        model.addAttribute("therapySession", therapyRepository.getSessionById(id));
        model.addAttribute("status", TherapyStatus.values());
        model.addAttribute("therapists", therapists = therapistRepository.findAllTherapists());

        return "views/therapy/edit";
    }

    @RequestMapping(value="/therapy/edit/{therapySession.id}", method = RequestMethod.POST)
    public String saveEditedTherapy(@Valid TherapySession therapySession,@PathVariable(value="therapySession.id") int id, final BindingResult bindingResult, final ModelMap model,RedirectAttributes attributes)
    {
        if(therapistRepository.therapistWorking(therapySession.getTherapistBSN(),therapySession.getStartDate())) {
            if (therapyRepository.checkAvailableDateForEdit(therapySession.getStartDate(), therapySession.getEndDate(), therapySession.getId(), therapySession.getTherapistBSN())) {
                //UPDATE SESSION
                therapyRepository.updateTherapySession(therapySession);
                attributes.addFlashAttribute("success", "De behandeling is gewijzigd.");

            } else {
                attributes.addFlashAttribute("error", "De behandeling kon niet worden ingepland, omdat er al een behandeling op dezelde datum en tijd staat gepland");
                return "redirect:/therapy/edit/" + id;
            }
        }
        else{
            attributes.addFlashAttribute("error", "De therapeut is op deze dag niet aanwezig.");
            return "redirect:/therapy/edit/" + id;
        }

        return "redirect:/therapy/edit/" + id;
    }

    @RequestMapping(value="/therapy/delete/{id}", method = RequestMethod.GET)
    public String removeSession(ModelMap model, @PathVariable int id) {
        model.addAttribute("therapySession", therapyRepository.getSessionById(id));

        return "views/therapy/delete";
    }


    @RequestMapping(value="/therapy/delete/{therapySession.id}", method = RequestMethod.POST)
    public String removeSession(@PathVariable(value="therapySession.id") int id,RedirectAttributes attributes)
    {
        int therapyId = therapyRepository.getSessionTherapyId(id);
        if(therapyRepository.therapySessionNumberIsOne(therapyId)){
            attributes.addFlashAttribute("success", "De sessie en behandeling is verwijderd.");
            therapyRepository.deleteSessionById(id);
            therapyRepository.deleteTherapyBySession(therapyId);
        }
        else{
            attributes.addFlashAttribute("success", "De sessie is verwijderd.");
            therapyRepository.deleteSessionById(id);
            therapyRepository.decreaseSessionNumber(therapyId);
        }

        return "redirect:/overview";
    }
}
