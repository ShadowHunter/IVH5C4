/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ivh5.controller;

import ivh5.model.FysioPractice;
import ivh5.repository.FysioPracticeRepository;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Rico
 */
@Controller
public class FysioPracticeController {
    private FysioPracticeRepository fysioPracticeRepository;
    
    //constructor
    @Autowired
    public FysioPracticeController(FysioPracticeRepository fysioPracticeRepository){
        this.fysioPracticeRepository = fysioPracticeRepository;
    }
    
    //GET create Fysiopractice
    @RequestMapping(value="/fysiopractice/create", method = RequestMethod.GET)
    public String CreateNewFysioPractice(final FysioPractice fysioPractice, final ModelMap model) {
        if(fysioPracticeRepository.checkCount() == 0){
            return "/views/fysiopractice/create";
        }
        else {
            model.addAttribute("error", "Er zijn al stamgegevens aangemaakt.");
            return "/views/fysiopractice/create";
        }
        
    }
    
    //POST create Fysiopractice
    @RequestMapping(value="/fysiopractice/create", method = RequestMethod.POST)
    public String validateAndSaveFysioPractice(@Valid FysioPractice fysioPractice, final BindingResult bindingResult, final ModelMap model,RedirectAttributes attributes) {
        if(bindingResult.hasErrors()){
            return "views/fysiopractice/create";
        }
        else {
            fysioPracticeRepository.create(fysioPractice);
            attributes.addFlashAttribute("success", "De stamgegevens zijn aangemaakt.");
        }
           
            return "redirect:/fysiopractice/overview"; 
    }
    
     //GET edit Fysiopractice
    @RequestMapping(value= "/fysiopractice/edit", method= RequestMethod.GET)     
    public String editFysioPractice(Model model) {
         if(fysioPracticeRepository.checkCount() != 0){
            model.addAttribute("fysiopractice", fysioPracticeRepository.findFysioPracticeById(1));
            return "views/fysiopractice/edit";     
         }
         else {
            model.addAttribute("error", "Geen stamgegevens gevonden.");
            return "views/fysiopractice/edit";     
         }
       
    }
        
     //POST edit Fysiopractice
    @RequestMapping(value="/fysiopractice/edit/{id}", method= RequestMethod.POST)
    public String editFysioPractice(@Valid FysioPractice fysiopractice, final BindingResult bindingResult, final ModelMap model,RedirectAttributes attributes) {
     
        fysioPracticeRepository.update(fysiopractice);  
        attributes.addFlashAttribute("success", "De stamgegevens zijn gewijzigd.");
        //System.out.println(id);
        return "redirect:/fysiopractice/overview";
        
    }
      
     //GET overview Fysiopractice
    @RequestMapping(value= "/fysiopractice/overview", method= RequestMethod.GET)     
    public String overviewFysioPractice(Model model) {
        if(fysioPracticeRepository.checkCount() != 0){
            model.addAttribute("fysiopractice", fysioPracticeRepository.findFysioPracticeById(1));
            return "views/fysiopractice/overview";
        }
        else {
            model.addAttribute("error", "Geen stamgegevens gevonden.");
            return "views/fysiopractice/overview";
        }

    }
    
}
