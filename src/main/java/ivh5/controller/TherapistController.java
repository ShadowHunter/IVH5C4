/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ivh5.controller;

import ivh5.model.Therapist;
import ivh5.repository.TherapistRepository;
import java.sql.SQLException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *Retrieves and sends data to the views. Retrieved data is sent to repository
 * 
 * @author lennard
 */
@Controller 
public class TherapistController {
    
    private Therapist therapist;
    private final TherapistRepository therapistRepository;
    
    @Autowired
    public TherapistController(TherapistRepository therapistRepository) {
       this.therapistRepository = therapistRepository;
    }
    
    /**
     * shows the form to create a new therapist
     * 
     * 
     * @param therapist The model object where data is mapped on
     * @param model The model from the Springframework
     * @return A view that contains the elements for creating a therapist
     * @Author Lennard Slabbekoorn
     */
    @RequestMapping(value="/createtherapist", method = RequestMethod.GET)
    public String showCreateMemberForm(final Therapist therapist, ModelMap model) {

        return "views/therapist/createtherapist";
    }
    
    /**
     * Shows a succes page after updating or deleting a therapist
     * @return A view that contains a succes message
     * @Author Lennard Slabbekoorn
     */
    //show succes message after creating a new therapist
    @RequestMapping("/changed")
    public String showChanged() {
        
        return "views/therapist/changed";
    }
    
    /**
     * Shows a succes page after creating a new therapist
     * @return A view that contains a succes message
     * @Author Lennard Slabbekoorn
     */
    //show succes message after update/delete
    @RequestMapping("/formsubmit")
    public String formSubmit() {
        
        return "views/therapist/formsubmit";
    }
    
    /**
     * Shows retrieved therapist data from database in a view
     * 
     * @param model The model from the Springframework itself
     * @return A view where the retrieved therapist data from the database is showed
     * @Author Lennard Slabbekoorn
     */
    //show list of therapists
    @RequestMapping(value= "/therapist", method= RequestMethod.GET)
    public String listTherapists(Model model) {
        
        model.addAttribute("therapists", therapistRepository.findAllTherapists());
    
        return "views/therapist/list";
    }
    
    /**
     * Shows the data of an individual therapist 
     * 
     * @param model The model of the Springframework itself
     * @param employeeNumber the employeeNumber of a therapist
     * @return A view where the therapist data of an individual therapist is 
     * showed and can be edited
     * @Author Lennard Slabbekoorn
     */
    //show the therapist to edit
    @RequestMapping(value= "/therapist/edit/{employeeNumber}", method= RequestMethod.GET)
    public String editTherapist(Model model, @PathVariable int employeeNumber) {
        
       model.addAttribute("therapist", therapistRepository.findTherapistByNumber(employeeNumber));
       
       return "views/therapist/edit";
    }
    
    /**
     * Retrieves data from the form (for creating a therapist) in the view and 
     * maps it on a therapist object
     * 
     * @param therapist The object therapist where the form data is mapped on
     * @param bindingResult The bindingresult of the therapist in the form on
     * the object therapist
     * @param model The model of the Springframework itself
     * @return a view with a succes message
     * @Author Lennard Slabbekoorn
     */
    //create new therapist
    @RequestMapping(value="/createtherapist", method= RequestMethod.POST)
    public String saveTherapist(@Valid Therapist therapist, final BindingResult bindingResult, final ModelMap model) {
        
        if (bindingResult.hasErrors()) {
            
            return "views/therapist/createtherapist";
        }
        
        else {
            
          therapistRepository.insertTherapist(therapist);
          
        }
        
        return "views/therapist/formsubmit";
    }
    
    /**
     * Retrieves data from the form (for updating a therapist) in the view and 
     * maps it on a therapist object 
     * 
     * @param therapist The object therapist where the form data is mapped on
     * @param bindingResult The bindingresult of the therapist in the form on
     * the object therapist
     * @param model The model of the Springframework itself
     * @return a view with a succes message 
     * @Author Lennard Slabbekoorn
     */
    //update therapist
    @RequestMapping(value="/therapist/edit/{employeeNumber}", method= RequestMethod.POST)
    public String updateTherapist(@Valid Therapist therapist, final BindingResult bindingResult, final ModelMap model) {
        if (bindingResult.hasErrors()) {
            
            return "views/therapist/edit";
        }
        
        else {
        therapistRepository.updateTherapist(therapist);
        }

        return "views/therapist/changed";
    }
    
    /**
     * Shows the therapist to be deleted in a view
     * 
     * @param model The model of the Springframework itself
     * @return a view with a succes message
     * @param employeeNumber the EmployeeNumber of a Therapist
     * @return A view thats shows the data of the therapist to be deleted 
     * @Author Lennard Slabbekoorn
     */
    //show the therapist to delete
    @RequestMapping(value= "/therapist/delete/{employeeNumber}", method= RequestMethod.GET)
    public String deleteTherapist(Model model, @PathVariable int employeeNumber) {
        
       model.addAttribute("therapist", therapistRepository.findTherapistByNumber(employeeNumber));
       
       return "views/therapist/delete";
    }
    
    /**
     * Calls a method in the repository to delete the specified therapist
     * 
     * @param employeeNumber the EmployeeNumber of a Therapist
     * @return A view that contains a succes mesage
     * @throws SQLException an Exception that is throwed when the database cannot
     * delete the specified therapist
     * @Author Lennard Slabbekoorn
     */
    //delete the therapist
    @RequestMapping(value="/therapist/delete/{employeeNumber}", method= RequestMethod.POST)
    public String deleteTherapist(@RequestParam("employeeNumber") int employeeNumber) throws SQLException {
        
        therapistRepository.deleteTherapist(employeeNumber);

        return "views/therapist/changed";
    }
}
