/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ivh5.controller;

import ivh5.model.Planning;
import ivh5.repository.TherapyOverviewRepository;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *Retrieves and sends data to the views. Retrieved data is sent to repository
 * 
 * @author Erik Koolen
 */
@Controller
public class TherapyOverviewController {
    
    private final TherapyOverviewRepository overviewRepo;
    
    
    @Autowired
    public TherapyOverviewController(TherapyOverviewRepository overviewRepo){
        this.overviewRepo = overviewRepo;
    }
    
    /**
     * shows the form to create a therapy overview
     * 
     * @param model the model from the springframework
     * @return A view that contains the elements for creating a therapy overview
     */
    @RequestMapping(value="/overview", method= RequestMethod.GET)
    public String overviewForm(Model model){       
        
        return "views/therapy/Overview";    
    }
    
    /**
     * get the submitted form values and map them on two strings for further
     * usage in the repository
     * 
     * @param startdate The string where the first form input is mapped on. This
     * should be a date with format MM/dd/yyyy
     * @param enddate The string where the second form input is mapped on. This
     * should be a date with format MM/dd/yyyy
     * @param model The model from the springframework
     * @return returns the overview with a filled in table depending on the two input strings
     * @throws ParseException Whenever the input string is not parsable this exception is thrown
     */
    @RequestMapping(value="/overview", method= RequestMethod.POST)
    public String overviewFormSubmit(String startdate, String enddate, Model model) throws ParseException{
        
        //get the startdate and enddate from the input field and format them to Date format yyyy-MM-dd.
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate formatStartDate =LocalDate.parse(startdate, formatter);
        LocalDate formatEndDate =LocalDate.parse(enddate, formatter);
        
        //formatEndDate +1
        LocalDate formatEndDate1 = formatEndDate.plusDays(1);
        
        model.addAttribute("startdate", startdate);
        model.addAttribute("enddate", enddate);
        
        //count all therapies between start and end date
        int cumulative = overviewRepo.findalltherapies(formatStartDate, formatEndDate1);
        
        //send the result to the model
        model.addAttribute("cumulative", cumulative);
        
        //get the planning from the database 
        List<Planning> planning = overviewRepo.findtherapy(formatStartDate, formatEndDate1);           
        
        //send the planning list to the model
        model.addAttribute("planning", planning);
        
        return "views/therapy/Overview";
    }
       
}
