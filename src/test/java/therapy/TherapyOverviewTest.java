/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package therapy;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ivh5.config.ApplicationContext;
import ivh5.model.Planning;
import ivh5.repository.TherapyOverviewRepository;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


/**
 *
 * @author Erik Koolen
 */
@SuppressWarnings("Duplicates")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= {ApplicationContext.class})
@WebAppConfiguration
public class TherapyOverviewTest {
    
    @Autowired
    WebApplicationContext context;
    
    MockMvc mockMvc;
    
    private Planning planning;
    private TherapyOverviewRepository overviewRepo;
    private final LocalDate startDate = LocalDate.now();
    private final LocalDate endDate = startDate.plusDays(1);
    
    @Before
    public void setup() {
        
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        
        planning = new Planning();
        planning.setPhysioName("Erik");
        
        overviewRepo = mock(TherapyOverviewRepository.class);
    }
    
    @Test
    public void testPlanningClass(){
        String name = planning.getPhysioName();
        assertEquals("Erik", name);
    }
    
    @Test
    public void testFindTherapy(){
        
        List<Planning> pList = new ArrayList<>();
        
        when(overviewRepo.findtherapy(startDate, endDate)).thenReturn(pList);
        assertEquals(pList, overviewRepo.findtherapy(startDate, endDate));
    }
    
    @Test
    public void testFindAllTherapies(){
        when(overviewRepo.findalltherapies(startDate, endDate)).thenReturn(1);
        assertEquals(1,overviewRepo.findalltherapies(startDate, endDate));
    }
    
    @Test
    public void testView() throws Exception{
        mockMvc.perform(get("/overview")                
        )
            .andExpect(view().name("views/therapy/Overview"));
    }
}
