package therapy;

import config.TestContext;
import ivh5.config.ApplicationContext;
import ivh5.model.TherapySession;
import ivh5.model.TherapyStatus;
import ivh5.repository.TherapyRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

/**
 * Created by rubie_000 on 30-10-2016.
 */
@SuppressWarnings("Duplicates")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContext.class, ApplicationContext.class})
@WebAppConfiguration
public class TherapyControllerTest {

    @Autowired
    WebApplicationContext context;

    MockMvc mockMvc;

    @MockBean
    private TherapyRepository therapyRepository;


    @Before
    public void setup() {
        System.out.println("---- setUp ----");

        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();
    }

    @After
    public void tearDown() {
        System.out.println("---- tearDown ----");
    }

    @Test
    public void PostEmptyForm() throws Exception {

        System.out.println("---- PostEmptyForm ----");

        mockMvc.perform(post("/therapy/create/")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .sessionAttr("therapySession", new TherapySession())
        )

                .andExpect(status().isOk())
                .andExpect(view().name("views/therapy/create"))
                .andExpect(model().attribute("therapySession", hasProperty("startDate", isEmptyOrNullString())))
                .andExpect(model().attribute("therapySession", hasProperty("endDate", isEmptyOrNullString())))
                .andExpect(model().attribute("therapySession", hasProperty("therapyStatus", isEmptyOrNullString())))
                .andExpect(model().attribute("therapySession", hasProperty("therapyCode1", equalTo(0))))
                .andExpect(model().attribute("therapySession", hasProperty("clientBSN", equalTo(0))))
                .andExpect(model().attribute("therapySession", hasProperty("therapistBSN", equalTo(0))))


                .andExpect(view().name("views/therapy/create"));


    }
    @Test
    public void verifiesTherapyCreatePageLoads() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/therapy/create"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(view().name("views/therapy/create"));
    }

    @Test
    public void postValidForm() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        Date startDate = sdf.parse("31-10-2016 12:00");
        Date endDate =  sdf.parse("30-11-2016 13:00");

        mockMvc.perform(post("/therapy/create")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("therapyStatus", TestContext.THERAPY_STATUS.toString())
                .param("therapyCode1", String.valueOf(TestContext.THERAPY_THERAPYCODE))
                .param("clientBSN", String.valueOf(TestContext.THERAPY_CLIENTBSN))
                .param("therapistBSN", String.valueOf(TestContext.THERAPY_THERAPISTBSN))
                .param("startDate", startDate.toString())
                .param("endDate", endDate.toString())
                .sessionAttr("therapySession", new TherapySession())
        )

                .andExpect(model().attribute("therapySession", hasProperty("therapyStatus", equalTo(TherapyStatus.GEPLAND))))
                .andExpect(model().attribute("therapySession", hasProperty("clientBSN", equalTo(123456789))))
                .andExpect(model().attribute("therapySession", hasProperty("therapistBSN", equalTo(987654321))))
                .andExpect(model().attribute("therapySession", hasProperty("therapyCode1", equalTo(1))))
                .andExpect(status().isOk());
    }

    @Test
    public void PostInvalidForm() throws Exception {

        mockMvc.perform(post("/therapy/create")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("therapyStatus", TestContext.THERAPY_STATUS.toString())
                .param("therapyCode1", String.valueOf(TestContext.THERAPY_THERAPYCODE))
                .param("clientBSN", String.valueOf(TestContext.THERAPY_CLIENTBSN))
                .param("therapistBSN", String.valueOf(TestContext.THERAPY_THERAPISTBSN))
//                .param("startDate", new Date().toString())
//                .param("endDate", new Date().toString())
                .sessionAttr("therapySession", new TherapySession())
        )
                .andDo(print())
                .andExpect(model().hasErrors());
    }
}
