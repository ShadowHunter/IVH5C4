package therapy;

import com.mysql.jdbc.PreparedStatement;
import config.TestContext;
import ivh5.model.Therapist;
import ivh5.model.TherapySession;
import ivh5.model.TherapyStatus;
import ivh5.repository.SessionRowMapper;
import ivh5.repository.TherapistRepository;
import ivh5.repository.TherapyRepository;
import org.junit.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.awt.print.Book;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Matchers.anyString;
/**
 * Created by rubie_000 on 30-10-2016.
 */

public class RepositoryTest {

    @Mock
    private Therapist mockTherapist;

    @Mock
    JdbcTemplate mockJdbcTemplate;

    @Mock
    DataSource mockDataSource;

    @Mock
    Connection mockConn;

    @Mock
    PreparedStatement mockPreparedStmnt;

    @Mock
    ResultSet mockResultSet;

    @InjectMocks
    private TherapyRepository therapyRepository;

    @Autowired
    private ApplicationContext applicationContext;

    private static TherapyRepository mockedTherapyRepository;
    private static TherapySession therapySession;

    public RepositoryTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.set(2016,Calendar.NOVEMBER,30);
        mockedTherapyRepository = mock(TherapyRepository.class);
        therapySession = new TherapySession(1, TherapyStatus.GEPLAND,cal.getTime(),cal.getTime(),1,123456789,123456786);

        //when(mockedTherapyRepository.create(therapySession)).thenReturn(therapySession);
        //when(mockedTherapyRepository.updateTherapySession(therapySession)).thenReturn(therapySession);
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws SQLException {
        System.out.println("---- setUp ----");

        MockitoAnnotations.initMocks(this);
    }

    @After
    public void tearDown() {
        System.out.println("---- tearDown ----");
    }


    @Test
    public void getSessionById()throws SQLException{
     mockedTherapyRepository.getSessionById(therapySession.getId());
    }

    @Test
    public void deleteSessionById() throws SQLException{
        mockedTherapyRepository.deleteSessionById(therapySession.getId());
        doNothing().when(mockedTherapyRepository).deleteSessionById(therapySession.getId());
    }

}
