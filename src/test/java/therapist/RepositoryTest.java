/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package therapist;

import com.mysql.jdbc.PreparedStatement;
import config.TestContext;
import ivh5.model.Therapist;
import ivh5.repository.TherapistRowMapper;
import ivh5.repository.TherapistRepository;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.mockito.InjectMocks;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import org.mockito.Mock;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author lenna
 */
public class RepositoryTest {
    
    @Mock
    private Therapist mockTherapist;

    @Mock
    JdbcTemplate mockJdbcTemplate;

    @Mock
    DataSource mockDataSource;

    @Mock
    Connection mockConn;

    @Mock
    PreparedStatement mockPreparedStmnt;

    @Mock
    ResultSet mockResultSet;
    
    @InjectMocks
    private TherapistRepository therapistRepository;

    @Autowired
    private ApplicationContext applicationContext;
    
    private static TherapistRepository mockedTherapistRepository;
    private static Therapist therapist;
    
    public RepositoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        mockedTherapistRepository = mock(TherapistRepository.class);
        therapist = new Therapist(TestContext.THERAPIST_BSN, TestContext.THERAPIST_EMPLOYEENUMBER, 
                TestContext.THERAPIST_HOURS, TestContext.THERAPIST_SALARY,
                TestContext.THERAPIST_NAME, TestContext.THERAPIST_ADDRESS, 
                TestContext.THERAPIST_POSTCODE, TestContext.THERAPIST_CITY,
                TestContext.THERAPIST_PHONE, TestContext.THERAPIST_EMAIL,
                TestContext.THERAPIST_DATEOFBIRTH, TestContext.THERAPIST_MONDAY,
                TestContext.THERAPIST_TUESDAY, TestContext.THERAPIST_WEDNESDAY, 
                TestContext.THERAPIST_THURSDAY, TestContext.THERAPIST_FRIDAY);
        
        when(mockedTherapistRepository.insertTherapist(therapist)).thenReturn(therapist);
        when(mockedTherapistRepository.updateTherapist(therapist)).thenReturn(therapist);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws SQLException {
        System.out.println("---- setUp ----");

        MockitoAnnotations.initMocks(this);
    }
    
    @After
    public void tearDown() {
        System.out.println("---- tearDown ----");
    }
    
    @Test
    public void addTherapist() throws SQLException {
        Therapist result = mockedTherapistRepository.insertTherapist(therapist);
        
        assertNotNull(result);
        assertEquals(therapist, result);
    }
    
    @Test
    public void updateTherapist() throws SQLException {
        Therapist result = mockedTherapistRepository.updateTherapist(therapist);
        
        assertNotNull(result);
        assertEquals(therapist, result);
    }
    
    @Test
    public void deleteTherapist() throws SQLException {
        mockedTherapistRepository.deleteTherapist(therapist.getEmployeeNumber());
        doNothing().when(mockedTherapistRepository).deleteTherapist(therapist.getEmployeeNumber());        
    }
    
    @Ignore
    @Test 
    public void findTherapist() throws SQLException {
        String sql = "SELECT * FROM physiotherapist WHERE EmployeeNumber=?";
        List therapists = new ArrayList<Therapist>();
        therapists.add(therapist);

        mockJdbcTemplate = mock(JdbcTemplate.class);
        mockDataSource = mock(DataSource.class);
        mockPreparedStmnt = mock(PreparedStatement.class);
        ResultSet resultSet = mock(ResultSet.class);
        mockJdbcTemplate.setDataSource(mockDataSource);

//      when(mockConn.createStatement()).thenReturn(mockPreparedStmnt);
        when(mockPreparedStmnt.executeQuery(anyString())).thenReturn(resultSet);
        when(mockJdbcTemplate.query(sql, new Object[]{TestContext.THERAPIST_EMPLOYEENUMBER}, new TherapistRowMapper() )).thenReturn(therapists);
        when(mockJdbcTemplate.query(anyString(), eq(new TherapistRowMapper() ))).thenReturn(therapists);


        TherapistRepository therapistRepository = new TherapistRepository();
        Therapist result = therapistRepository.findTherapistByNumber(TestContext.THERAPIST_EMPLOYEENUMBER);

        Assert.assertEquals(result, therapists.get(0));
    }
}
        

