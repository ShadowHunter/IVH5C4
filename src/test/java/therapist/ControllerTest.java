/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package therapist;

import config.TestContext;
import ivh5.config.ApplicationContext;
import ivh5.model.Therapist;
import ivh5.repository.TherapistRepository;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author lenna
 */
@SuppressWarnings("Duplicates")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContext.class, ApplicationContext.class})
@WebAppConfiguration
public class ControllerTest {
    
    @Autowired
    WebApplicationContext context;

    MockMvc mockMvc;

    @MockBean
    private TherapistRepository therapistRepository;


    @Before
    public void setup() {
        System.out.println("---- setUp ----");

        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();
    }

    @After
    public void tearDown() {
        System.out.println("---- tearDown ----");
    }

    @Test
    public void PostEmptyForm() throws Exception {

        System.out.println("---- PostEmptyForm ----");

        mockMvc.perform(post("/createtherapist")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .sessionAttr("therapist", new Therapist())
        )

            .andExpect(status().isOk())
            .andExpect(view().name("views/therapist/createtherapist"))
                
            .andExpect(model().attribute("therapist", hasProperty("bsn", equalTo(0))))
            .andExpect(model().attribute("therapist", hasProperty("name", isEmptyOrNullString())))
            .andExpect(model().attribute("therapist", hasProperty("dateOfBirth", isEmptyOrNullString())))
            .andExpect(model().attribute("therapist", hasProperty("salary", equalTo(0.0))))
            .andExpect(model().attribute("therapist", hasProperty("hours", equalTo(0))))
            .andExpect(model().attribute("therapist", hasProperty("address", isEmptyOrNullString())))
            .andExpect(model().attribute("therapist", hasProperty("postcode", isEmptyOrNullString())))
            .andExpect(model().attribute("therapist", hasProperty("city", isEmptyOrNullString())))
            .andExpect(model().attribute("therapist", hasProperty("phone", isEmptyOrNullString())))
            .andExpect(model().attribute("therapist", hasProperty("email", isEmptyOrNullString())))
            .andExpect(model().attribute("therapist", hasProperty("monday", equalTo(false))))
            .andExpect(model().attribute("therapist", hasProperty("tuesday", equalTo(false))))
            .andExpect(model().attribute("therapist", hasProperty("wednesday", equalTo(false))))
            .andExpect(model().attribute("therapist", hasProperty("thursday", equalTo(false))))
            .andExpect(model().attribute("therapist", hasProperty("friday", equalTo(false))))
        
            .andExpect(view().name("views/therapist/createtherapist"));
        
                 
    }
    
    @Test
    public void incompleteForm() throws Exception {

        mockMvc.perform(post("/createtherapist")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                
                .param("name", TestContext.THERAPIST_NAME)
                .param("postcode", TestContext.THERAPIST_POSTCODE)
                .sessionAttr("therapist", new Therapist())
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("views/therapist/createtherapist"))
                .andExpect(model().attribute("therapist", hasProperty("name", equalTo(TestContext.THERAPIST_NAME))))
                .andExpect(model().attribute("therapist", hasProperty("postcode", equalTo(TestContext.THERAPIST_POSTCODE))))
                .andExpect(model().hasErrors())
                .andExpect(view().name("views/therapist/createtherapist"));
    }
    
    @Test
    public void postValidForm() throws Exception {

        mockMvc.perform(post("/createtherapist")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("name", TestContext.THERAPIST_NAME)
                .param("dateOfBirth", TestContext.THERAPIST_DATEOFBIRTH)
                .param("address", TestContext.THERAPIST_ADDRESS)
                .param("postcode", TestContext.THERAPIST_POSTCODE)
                .param("city", TestContext.THERAPIST_CITY)
                .param("phone", TestContext.THERAPIST_PHONE)
                .param("email", TestContext.THERAPIST_EMAIL)
                .sessionAttr("therapist", new Therapist())
        )
                .andExpect(model().attribute("therapist", hasProperty("bsn", equalTo(0))))
                .andExpect(model().attribute("therapist", hasProperty("salary", equalTo(0.0))))
                .andExpect(model().attribute("therapist", hasProperty("hours", equalTo(0))))
                .andExpect(model().attribute("therapist", hasProperty("monday", equalTo(false))))
                .andExpect(model().attribute("therapist", hasProperty("tuesday", equalTo(false))))
                .andExpect(model().attribute("therapist", hasProperty("wednesday", equalTo(false))))
                .andExpect(model().attribute("therapist", hasProperty("thursday", equalTo(false))))
                .andExpect(model().attribute("therapist", hasProperty("friday", equalTo(false))))
                .andExpect(status().isOk());
    }
    
}
