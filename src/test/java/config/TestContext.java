/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package config;



import ivh5.model.TherapyStatus;
import ivh5.repository.TherapistRepository;
import ivh5.repository.TherapyRepository;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author lenna
 */
@Configuration
public class TestContext {

    public static final int THERAPIST_BSN = 123456789;
    public static final int THERAPIST_EMPLOYEENUMBER = 1005;
    public static final String THERAPIST_NAME = "name";
    public static final String THERAPIST_DATEOFBIRTH = "01-01-2000";
    public static final double THERAPIST_SALARY = 9999.99;
    public static final int THERAPIST_HOURS = 99;
    public static final String THERAPIST_ADDRESS = "address 0";
    public static final String THERAPIST_POSTCODE = "9999AA";
    public static final String THERAPIST_CITY = "city";
    public static final String THERAPIST_PHONE = "0612345678";
    public static final String THERAPIST_EMAIL = "dummy1@mail.com";
    public static final boolean THERAPIST_MONDAY = true;
    public static final boolean THERAPIST_TUESDAY = true;
    public static final boolean THERAPIST_WEDNESDAY = true;
    public static final boolean THERAPIST_THURSDAY = true;
    public static final boolean THERAPIST_FRIDAY = true;


    //----------------------------------------------------
//    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
//    Date startDate = sdf.parse("31-10-2900");
//    Date endDate =  sdf.parse("31-10-1900");

    //long time = 210000;
//    public static final Date THERAPY_STARTDATE =  new Date().setTime();
//    public static final Date THERAPY_ENDDATE =  new Date().getTime();
    public static final TherapyStatus THERAPY_STATUS=  TherapyStatus.GEPLAND;
    public static final int THERAPY_THERAPYCODE= 1;
    public static final int THERAPY_CLIENTBSN = 123456789;
    public static final int THERAPY_THERAPISTBSN = 987654321;


    @Bean
    public TherapistRepository therapistRepository() {
        return Mockito.mock(TherapistRepository.class);
    }

    @Bean
    public TherapyRepository therapyRepository() {
        return Mockito.mock(TherapyRepository.class);
    }
}
